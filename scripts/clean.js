var utils = require("./utils");

utils.deleteFolderRecursive(".cache");
utils.deleteFolderRecursive("build");
utils.deleteFolderRecursive("mobile/www/build");
