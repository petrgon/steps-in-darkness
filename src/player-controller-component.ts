import * as ECSA from '../libs/pixi-component';
import { ComponentNames, Attributes, Messages } from './constants';
import Dynamics from './abstracts/dynamics';
import { GameModel } from './game-model';
import GameFactory from './game-factory';

export default class PlayerInputController extends ECSA.Component {

    private speed = 10;
    model: GameModel;
    factory: GameFactory;
    private _jumping: boolean;
    cmp: ECSA.KeyInputComponent;
    cmpDynamics: Dynamics;
    gameOver: boolean;

    get jumping() {
        return this._jumping;
    }
    set jumping(jumping: boolean) {
        this._jumping = jumping;
    }

    constructor() {
        super();
        this._name = ComponentNames.PLAYER_CONTROLLER;
    }

    onInit() {
        super.onInit();
        this.subscribe(Messages.GAME_OVER, Messages.GAME_START);
        this.model = this.scene.getGlobalAttribute<GameModel>(Attributes.GAME_MODEL);
        this.factory = this.scene.getGlobalAttribute<GameFactory>(Attributes.GAME_FACTORY);
        this._jumping = false;
        this.gameOver = false;
    }

    onUpdate(delta: number, absolute: number) {
        if (this.cmp === undefined || this.cmpDynamics === undefined) {
            this.cmp = this.scene.stage.findComponentByName<ECSA.KeyInputComponent>(ECSA.KeyInputComponent.name);
            this.cmpDynamics = this.owner.findComponentByName<Dynamics>(ComponentNames.DYNAMICS);
            this.cmpDynamics.disabled = true;
        }
        if (this.cmp.isKeyPressed(ECSA.Keys.KEY_LEFT)) {
            this.cmpDynamics.force(new ECSA.Vector(-this.speed, 0));
        }

        if (this.cmp.isKeyPressed(ECSA.Keys.KEY_RIGHT)) {
            this.cmpDynamics.force(new ECSA.Vector(+this.speed, 0));
        }
        if (!this._jumping)
            if (this.cmp.isKeyPressed(ECSA.Keys.KEY_UP)) {
                this.cmpDynamics.force(new ECSA.Vector(0, -this.speed * 75));
                this.sendMessage(Messages.PLAY_TONE);
                this._jumping = true;
            }
        if ((this.cmp.isKeyPressed(ECSA.Keys.KEY_F) || this.cmp.isKeyPressed(ECSA.Keys.KEY_R))
            && !this.gameOver) {
            this.sendMessage(Messages.GAME_RESTART)
            this.factory.resetGame(this.scene);
        }
        if (Math.abs(this.cmpDynamics.velocity.y) > 10e-6)
            this._jumping = true;
    }

    onMessage(msg: ECSA.Message) {
        if (msg.action === Messages.GAME_OVER) {
            this.cmpDynamics.disabled = true;
            this.gameOver = true;
        }
        if (msg.action === Messages.GAME_START)
            this.cmpDynamics.disabled = false;
    }

}