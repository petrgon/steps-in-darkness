import * as ECSA from '../../libs/pixi-component';
import { ComponentNames } from '../constants';

export class RectangleCollider extends ECSA.Component {

    private _isTrigger: boolean;
    set isTrigger(val: boolean) {
        this._isTrigger = val;
    }

    get isTrigger() {
        return this._isTrigger;
    }

    constructor() {
        super();
        this.isTrigger = false;
        this._name = ComponentNames.COLLIDER;
    }

    public collide(other: RectangleCollider) {
        let meX = this.owner.pixiObj.position.x
        let meY = this.owner.pixiObj.position.y
        let meWidth = this.owner.pixiObj.width;
        let meHeight = this.owner.pixiObj.height;

        let otherX = other.owner.pixiObj.position.x
        let otherY = other.owner.pixiObj.position.y
        let otherWidth = other.owner.pixiObj.width;
        let otherHeight = other.owner.pixiObj.height;

        if (meX < otherX + otherWidth &&
            meX + meWidth > otherX &&
            meY < otherY + otherHeight &&
            meY + meHeight > otherY) {
            return true;
        }
        return false;
    }

    public onCollision(other: RectangleCollider) {
        //do nothing
    }
    public onTrigger(other: RectangleCollider) {
        //do nothing
    }
}