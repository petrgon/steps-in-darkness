import * as ECSA from '../libs/pixi-component';
import { BFlags, ComponentNames, Names, Tags } from './constants';
import { RectangleCollider } from './abstracts/rectangle-collider';

export class CollisionManager extends ECSA.Component {
    colider = new Array<ECSA.GameObject>();

    constructor() {
        super();
    }

    onInit() {
        super.onInit();
        this.subscribe(ECSA.Messages.OBJECT_ADDED, ECSA.Messages.OBJECT_REMOVED);
        this.colider = this.scene.findObjectsByTag(Tags.COLLIDER);
    }

    onMessage(msg: ECSA.Message) {
        if (msg.action === ECSA.Messages.OBJECT_ADDED || msg.action === ECSA.Messages.OBJECT_REMOVED) {
            this.colider = new Array<ECSA.GameObject>();
            this.colider = this.scene.findObjectsByTag(Tags.COLLIDER);
        }
    }
    onUpdate(delta: number, absolute: number) {
        for (let i = 0; i < this.colider.length; i++) {
            let gameObjectCollider = this.colider[i].findComponentByName<RectangleCollider>(ComponentNames.COLLIDER);
            for (let j = i + 1; j < this.colider.length; j++) {
                let otherGameObjectCollider = this.colider[j].findComponentByName<RectangleCollider>(ComponentNames.COLLIDER);
                if (gameObjectCollider.collide(otherGameObjectCollider)) {
                    if (otherGameObjectCollider.isTrigger)
                        otherGameObjectCollider.onTrigger(gameObjectCollider);
                    if (gameObjectCollider.isTrigger)
                        gameObjectCollider.onTrigger(otherGameObjectCollider);
                }
            }

        }
    }

    collidePhysics(obj: ECSA.GameObject) {
        let objCollider = obj.findComponentByName<RectangleCollider>(ComponentNames.COLLIDER);
        if (objCollider === undefined || objCollider === null)
            return null;
        for (let i = 0; i < this.colider.length; i++) {
            if (this.colider[i] === obj)
                continue;
            let gameObjectCollider = this.colider[i].findComponentByName<RectangleCollider>(ComponentNames.COLLIDER);
            if (objCollider.collide(gameObjectCollider)) {
                if (!objCollider.isTrigger && !gameObjectCollider.isTrigger) {
                    objCollider.onCollision(gameObjectCollider);
                    gameObjectCollider.onCollision(objCollider);
                    return gameObjectCollider;
                }
            }
        }
        return null;
    }
}