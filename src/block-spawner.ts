import * as ECSA from '../libs/pixi-component';
import { Attributes, Messages } from './constants';
import { GameModel } from './game-model';
import GameFactory from './game-factory';


export class BlockSpawner extends ECSA.Component {

    spawnFrequency;
    model: GameModel;
    factory: GameFactory;
    lastSpawned: number;
    active: boolean;

    onInit() {
        super.onInit();
        this.lastSpawned = 0;
        this.spawnFrequency = 0;
        this.active = false;
        this.subscribe(Messages.GAME_OVER, Messages.GAME_START);
        this.model = this.scene.getGlobalAttribute<GameModel>(Attributes.GAME_MODEL);
        this.factory = this.scene.getGlobalAttribute<GameFactory>(Attributes.GAME_FACTORY);

        this.spawnFrequency = this.model.spawning_frequency;
    }

    onUpdate(delta: number, absolute: number) {
        if (this.active && absolute - this.lastSpawned > 1000 / this.spawnFrequency) {
            this.factory.createBlock(this.owner);
            //this.sendMessage(Messages.PLAY_TONE);
            this.lastSpawned = absolute;
        }
    }

    onMessage(msg: ECSA.Message) {
        if (msg.action === Messages.GAME_OVER) {
            this.active = false;
        }
        if (msg.action === Messages.GAME_START) {
            this.active = true;
        }
    }
}