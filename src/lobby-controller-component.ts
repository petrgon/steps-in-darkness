import * as ECSA from '../libs/pixi-component';
import { ComponentNames, Attributes, Messages } from './constants';
import Dynamics from './abstracts/dynamics';
import { GameModel } from './game-model';
import GameFactory from './game-factory';

export default class LobbyController extends ECSA.Component {

    private speed = 10;
    model: GameModel;
    factory: GameFactory;
    cmp: ECSA.KeyInputComponent;
    gameStarted: boolean;

    constructor() {
        super();
        this._name = ComponentNames.LOBBY_CONTROLLER;
    }

    onInit() {
        super.onInit();
        this.gameStarted = false;
    }

    onUpdate(delta: number, absolute: number) {
        if (this.cmp === undefined) {
            this.cmp = this.scene.stage.findComponentByName<ECSA.KeyInputComponent>(ECSA.KeyInputComponent.name);
        }
        if (!this.gameStarted &&
            (this.cmp.isKeyPressed(ECSA.Keys.KEY_SPACE) ||
                this.cmp.isKeyPressed(ECSA.Keys.KEY_RIGHT) ||
                this.cmp.isKeyPressed(ECSA.Keys.KEY_LEFT) ||
                this.cmp.isKeyPressed(ECSA.Keys.KEY_UP) ||
                this.cmp.isKeyPressed(ECSA.Keys.KEY_DOWN))) {

            this.sendMessage(Messages.GAME_START);
            this.gameStarted = true;
            this.owner.pixiObj.destroy();
        }
    }
}